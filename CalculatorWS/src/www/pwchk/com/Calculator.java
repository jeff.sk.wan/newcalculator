package www.pwchk.com;

import java.io.File;
import java.io.IOException;

public class Calculator {
	public double sum (double x, double y) {
		return x+y;
	}
	public double subtract (double x, double y) throws IOException {
		
		File tempDir;
        tempDir = File.createTempFile("", ".");
        tempDir.delete();
        tempDir.mkdir();  // Noncompliant
        return x-y;
	}
	public double multiply (double x, double y) {
		return x*y;
	}
	public double divide (double x, double y) {
		return x/y;
	}
}
